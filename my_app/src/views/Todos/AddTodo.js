import React from "react";
import {toast} from 'react-toastify';
import './AddTodo.scss'
class AddTodo extends React.Component {
    state = {
        title: ''
    }
    handleOnchangeTitle = (event) => {
        this.setState({
            title: event.target.value
        })
    }
    handelAddTodo = () => {
        if (!this.state.title) {
        //      if (this.state.title)  => false
            //      if (!this.state.title)  => true
            // alert('missing title')
            toast.error("missing title!")
            return

        }
        let todo = {
            id: Math.floor(Math.random() * 10000),
            title: this.state.title
        }
        this.props.addNewTodo(todo);
        this.setState({
            title: ''
        })
    }

    render() {
        let {title} = this.state;
        return (

            <>
                <div className="add-todo">
                    <input type="text" value={title}
                           onChange={(event) => this.handleOnchangeTitle(event)}
                    />
                    <button type="text" className="add"
                            onClick={() => this.handelAddTodo()}
                    > Add
                    </button>

                </div>

            </>

        )
    }
}

export default AddTodo;