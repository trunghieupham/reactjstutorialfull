import React from 'react';
import ChildComponent from "./ChildComponent";
import AddComponent from "./AddComponent";
// import {ReactComponent} from "*.svg";

class Mycomponent extends React.Component {

    // 2 components : class component - function component( function / arrow)
    // jsx :chung ta co the code javacript ngay  trong html-mot ham javacript co the tra ra html


    /*
    state

     */
    state = {
        arrJobs: [
            {
                id: '1',
                title: 'dev',
                salary: '500'
            },
            {
                id: '2',
                title: 'tester',
                salary: '400'
            },
            {
                id: '3',
                title: 'PM',
                salary: '1000'
            }
        ]
    }

    addNewJob=(job)=>{
        // let currentJobs =this.state.arrJobs;
        // currentJobs.push(job);
        this.setState({
            // arrJobs:this.state.arrJobs.push(job)
            arrJobs:[...this.state.arrJobs,job]
        //    cach 2
        //     arrJobs:currentJobs

        })
        console.log('check job from parent:',job)
    }
    deleteAJob=(job)=>{
        let currentJobs =this.state.arrJobs;
        currentJobs = currentJobs.filter(item => item.id !== job.id);
        this.setState({
            arrJobs: currentJobs
        })
    }
    hanldeOnchangeName = (event) => {
        this.setState({
            name: event.target.value
        })
    }
    render() {
        let name = 'Hieu Pham';
        return (
            <>
                {/*<div className="first">*/}
                {/*    {console.log('my name is', name)}*/}
                {/*    <div> hello my component by me</div>*/}
                {/*    <h1>My name is {name} </h1>*/}


                {/*</div>*/}
                {/*<div className="second">*/}
                {/*    <p> ---------------- STATE-----------------</p>*/}
                {/*    <h3>Get name by state {this.state.name} </h3>*/}
                {/*    <p> ---------------- setState-----------------</p>*/}
                {/*    <input value={this.state.name} type="text" onChange={(event) => this.hanldeOnchangeName(event)}/>*/}
                {/*</div>*/}
                {/*<div className="third">*/}
                {/*    <button onClick={() => this.handleButton()}> click me</button>*/}
                {/*</div>*/}

                <AddComponent
                    addNewJob={this.addNewJob}
                />
                <ChildComponent

                    arrJobs={this.state.arrJobs}
                    deleteAJob={this.deleteAJob}
                />


            </>
        )
    }
}

export default Mycomponent;