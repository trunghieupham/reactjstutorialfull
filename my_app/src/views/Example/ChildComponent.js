import React from 'react';
import './Demo.scss'
// import {ReactComponent} from "*.svg";
/*
khi chung ta chi can render data tu cha sang con thi su dung function component thay vi class component
 */

//
class ChildComponent extends React.Component {
    state = {
        showjobs: false
    }
    handleShowHide = () => {
        this.setState({
            showjobs: !this.state.showjobs
        })
    }

    handleOnclickDelete = (job) => {
        console.log('>>>handleOnclickDelete', job)
        this.props.deleteAJob(job)

    }

    render() {
        let {arrJobs} = this.props;
        let job = '';
        let {showjobs} = this.state;
        let check = showjobs === true ? 'showjobs = true' : 'showjobs = false'
        return (
            <>
                <h4>-----child component-----</h4>
                {showjobs === false ? <div className="show">
                        <button className="btn-show" onClick={() => this.handleShowHide()}>show</button>
                    </div>
                    :
                    <>
                        <div className="job-lists">
                            <h4>------props Outputting Lists and conditonal ouput ------</h4>
                            {
                                job = arrJobs.map((item, id) => {
                                    return (
                                        <div key={item.id}>
                                            {item.title}-{item.salary} <span
                                            onClick={() => this.handleOnclickDelete(item)}>X</span>
                                        </div>
                                    )

                                })

                            }
                            {console.log('>>>check map array', job)}

                        </div>
                        <div>
                            <button onClick={() => this.handleShowHide()}>hide</button>
                        </div>
                    </>
                }
            </>
        )
    }
}

//
// const ChildComponent = (props) => {
//     console.log('>>>check child props:', props)
//     let {arrJobs} = props;
//     return (
//         <div>
//             <h4>----------hello arrow function ------------</h4>
//             <div></div>
//             {
//                 arrJobs.map((item, index) => {
//                     if (item.salary >= 500) {
//                         return (
//                             <div className="job-lists">
//                                 {item.title}- {item.salary}$
//                             </div>
//                         )
//                     }
//
//                 })
//             }
//         </div>
//     )
//
//
// }
export default ChildComponent;