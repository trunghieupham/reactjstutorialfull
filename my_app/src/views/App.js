import logo from './logo.svg';
import './App.scss';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';
import Mycomponent from "./Example/Mycomponent";
import ListTodo from "./Todos/ListTodo";
function App() {
  return (
    <div className="App">
      <img src={logo} className="App-logo" alt="logo" />
      <p>
        Simple TODO Apps with React.js
      </p>
      {/*<header className="App-header">*/}


      {/*  /!*<Mycomponent/>*!/*/}

      {/*</header>*/}
      <ListTodo/>
        <ToastContainer
            position="top-right"
            autoClose={5000}
            hideProgressBar={false}
            newestOnTop={false}
            closeOnClick
            rtl={false}
            pauseOnFocusLoss
            draggable
            pauseOnHover
        />
    </div>
  );
}

export default App;
